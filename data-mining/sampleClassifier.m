function [cls, params] = sampleClassifier(datadir, opts, reload)
    newInValidation = 3; % # of people not included in the training set
    oldPercInValidation = 0.1; % split ratio of the rest
    singVects = 10; % # of largest singular vectors to use in the classifier
    alg = 'svd'; % other options: 'hosvd3'
    hosvdCut = []; % # of vector to cut from hosvd
    alphaNew = [];  % # of people for training alpha parameter
    alphaOldPerc = []; % split ratio of training set between classifier and alpha
    if exist('opts')
        if isfield(opts, 'alg')
            alg = opts.alg;
        end
        if isfield(opts, 'n')
            newInValidation = opts.n;
        end
        if isfield(opts, 'p')
            oldPercInValidation = opts.p;
        end
        if isfield(opts, 'k')
            singVects = opts.k;
        end
        if isfield(opts, 'c')
            hosvdCut = opts.c;
        end
        if isfield(opts, 'an')
            alphaNew = opts.an;
        end
        if isfield(opts, 'ap')
            alphaOldPerc = opts.ap;
        end
    end
    
    rand('state', [2 3 4 5 42]); % for repeatable results
    
    page_screen_output(0, 'local');

    persistent data pids cids loaded = 0;
    
    % load data
    if ~loaded || exist('reload')
        DATABASE = 'train_64_48.txt';
        fprintf('Loading training data...\t'); fflush(stdout);
        try
            load([datadir '/generated/' DATABASE]);
        catch
            fprintf('FAILED\nCreating data file from raw image data...\n');
            fflush(stdout);
            [data, pids, cids] = ...
                loadTrainSet(datadir, 0.1, 'bicubic', DATABASE);
        end
        fprintf('DONE\n'); fflush(stdout);
        loaded = 1;
    end
    
    fprintf('Set size: %d\n', size(data, 2)); fflush(stdout);
    
    % group the data into 2 dimensional (person x class) cell
    G = groupData(data, pids, cids);
    
    % remove person 24 as there are only 2 images in one category
    p24 = G(24,:);
    G = G([1:23 25:26],:);
    
    %[G, ~] = splitData(G, 0, 0.9, 0);
    
    fprintf("# of people: %d\n", size(G, 1));
    fprintf("# of classes: %d\n", size(G, 2));
    fprintf("# of examples per (person, class) pair:\n");
    disp(cellfun(@(x) size(x, 2), G));
    fflush(stdout);
    
    % split it into training and validation set
    fprintf('\nSplitting data into train and validation sets:\n');
    fprintf('# unseen in validation: %d\n', newInValidation);
    fprintf('%% of seen in validation: %.2f\n', oldPercInValidation);
    fflush(stdout);
    [train, valid] = splitData(G, newInValidation, oldPercInValidation);
    %valid = cat(1, valid, p24);
    fprintf("# of examples per (person, class) pair in train set:\n");
    disp(cellfun(@(x) size(x, 2), train));
    fprintf("# of examples per (person, class) pair in validation set:\n");
    disp(cellfun(@(x) size(x, 2), valid));
    
    % train classfier
    fprintf('Training classifier:\n# of singular vectors: %d\n', singVects);
    fflush(stdout);
    switch alg
        case 'svd'
            params = trainSVDClassifier(
            train, singVects, alphaNew, alphaOldPerc);
            cls = @(x) classifySVD(params, x, 0);
        case 'hosvd3'
            params = trainHOSVD3Classifier(
                train, singVects, alphaNew, alphaOldPerc, hosvdCut);
            cls = @(x) classifyHOSVD3(params, x, 0);
        case 'hosvd4'
            params = trainHOSVD4Classifier(
                train, singVects, alphaNew, alphaOldPerc, hosvdCut);
            cls = @(x) classifyHOSVD4(params, x, 0);
    end
    
    % check classifier errors
    fprintf('Accuracy on training set:\n'); fflush(stdout);
    checkClassifier(cls, squashPeople(train));
    fprintf('Accuracy on validation set:\n'); fflush(stdout);
    checkClassifier(cls, squashPeople(valid));
end
