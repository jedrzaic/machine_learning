function [ind, dist] = classifyHOSVD3(params, tests, conf)
    n = size(params.Q, 3);
    ntest = size(tests, 2);
    dist = zeros(n, ntest);
    for i = 1:n
        Qi = params.F*params.Q(:,:,i);
        dist(i,:) = norm(tests - Qi * (Qi' * tests), 2, 'cols');
    end
    [~, ind] = min(dist, [], 1);
    if exist('conf')
        dist = exp(-params.alpha*(dist - min(dist)));
    end
end
