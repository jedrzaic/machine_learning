function [fnames] = loadTestSet (path, scale, outfile, method, start, blksize)
    if exist('method') == 0
        method = 'bicubic';
    end
    
    if exist('blksize') == 0
        blksize = 20000;
    end
    
    if exist('start') == 0
        start = 1;
    end
    
    cblk = start;
    start = (start-1) * blksize + 1;
    tests = glob([path '/test/*.jpg']);
    n = length(tests);
    blks = ceil(n / blksize);
    fnames = cell(blks, 1);
    tnames = cell(blksize, 1);
    errs = 0;
    
    %load images
    for i = start:n
        fprintf('Processing image %d / %d (%s)\n', i, n, tests{i});
        fflush(stdout);
        I = imread(tests{i});
        try 
            I = rgb2gray(imresize(I, scale, method));
        catch
            fprintf('Error - setting image to 0\n'); fflush(stdout);
            I = zeros(size(data,1), 1);
            errs = errs + 1;
        end
        if (i == 1)
            data = zeros(numel(I), blksize);
        end
        data(:,mod(i-1, blksize)+1) = I(:);
        [~, f, e] = fileparts(tests{i});
        tnames(mod(i-1, blksize)+1) = [f e];
        
        if mod(i, blksize) == 0
            fnames{cblk} = [path '/generated/' outfile sprintf('.%d', cblk)];
            saveData(data, tnames, fnames{cblk});
            cblk = cblk + 1;
        elseif i == n
            fnames{cblk} = [path '/generated/' outfile sprintf('.%d', cblk)];
            sz = n - (blks-1)*blksize;
            saveData(data(:, 1:sz), tnames(1:sz), fnames{cblk});
        end
    end
    
    fprintf('DONE!\nErrors: %d\n', errs); fflush(stdout);
end

function [] = saveData(data, tnames, outfile)
    fprintf('Savind data block to %s\n', outfile); fflush(stdout);
    [status, msg] = mkdir(path, 'generated');
    save(outfile, 'data', 'tnames');
end
