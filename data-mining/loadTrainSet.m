function [data, pids, cids] = loadTrainSet(path, scale, method, outfile)
    if exist('method') == 0
        method = 'bicubic';
    end
    
    % load the metadata file
    METAFILE = 'driver_imgs_list.csv';
    fprintf('Reading %s\n', METAFILE);
    metadata = csv2cell([path '/' METAFILE], 1);
    n = size(metadata, 1);
    
    % normalize person ids and category ids to start from 1
    [~, ~, pids] = unique(metadata(:,1));
    [~, ~, cids] = unique(metadata(:,2));
    
    %load images
    for i = 1:n
        fprintf('Processing image %d / %d\n', i, n);
        fflush(stdout);
        impath = [path '/train/' metadata{i,2} '/' metadata{i,3}];
        I = imread(impath);
        try 
            I = rgb2gray(imresize(I, scale, method));
        catch
            I = zeros(size(data,1), 1);
        end
        if (i == 1)
            data = zeros(numel(I), n);
        end
        data(:,i) = I(:);
    end

    %save to file
    if exist('outfile') ~= 0
        outpath = [path '/generated/' outfile];
        fprintf('Saving data to %s\n', outpath);
        fflush(stdout);
        [status, msg] = mkdir(path, 'generated');
        save(outpath, 'data', 'pids', 'cids');
    end
end
