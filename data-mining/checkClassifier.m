function [total, perclass, logloss] = checkClassifier(clsfunc, tests)
    n = length(tests);
    perclass = zeros(n, 1);
    tlen = 0;
    total = 0;
    logloss = 0;
    
    for i = 1:n
        fprintf("Checking class %2d/%2d  |  ", i, n);
        fflush(stdout);
        if size(tests{i}, 2) > 0
            [pred, conf] = clsfunc(tests{i});
            conf = conf ./ sum(conf);
            logloss = logloss + sum(log(conf(i,:)));
            cls = sum((1:n)' == pred, 2);
        else
            cls = zeros(n, 1);
        end
        fprintf('%d\t', cls);
        tsize = size(tests{i}, 2);
        perclass(i) = cls(i) / max(tsize, 1);
        fprintf('|  %2.2f%%\n', perclass(i) * 100);
        total = total + cls(i);
        tlen = tlen + tsize;
    end
    
    logloss = -logloss / tlen;
    
    total = total / tlen;
    
    fprintf('Total: %.2f%%\n', total * 100);
    fprintf('logloss: %f\n', logloss);
    fflush(stdout);
end
