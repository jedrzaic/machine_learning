function [params] = trainHOSVD3Classifier(M, k, n, p, c)
    if ~exist('n') || length(n) == 0
        n = 3;
    end
    if ~exist('p') || length(p) == 0
        p = 0.1;
    end
    if ~exist('c')
        c = [];
    end
    [train, tAlpha] = splitData(M, n, p);
    train = squashPeople(train);
    tAlpha = squashPeople(tAlpha);
    n = length(train);
    T = zeros(size(train{1},1), k, n);
    for i = 1:n
        fprintf("Training class %d/%d", i, n); fflush(stdout);
        [T(:,:,i), S, ~, flag] = svds(train{i}, k);
        if flag == 0
            fprintf('\tConverged. min/max singular value ratio: %f\n', ...
                    S(k,k)/S(1,1)); fflush(stdout);
        else
            fprintf('\tFailed to converge!\n'); fflush(stdout);
        end
    end
    
    [S, F, ~, H] = hosvd(T);
    if length(c) == 0
        c = size(S);
    end
    c = min(c, size(S));
    S = S(1:c(1), 1:c(2), 1:c(3));
    F = F(:, 1:c(1));
    H = H(:, 1:c(3));
    
    Q = tmul(S, H, 3); 
    
    for i = 1:n
        [Q(:,:,i), ~] = qr(Q(:,:,i), '0');
    end
    
    params = struct('S', S, 'F', F, 'Q', Q, 'alpha', 1);
    
    fprintf('Calculating optimal distance loss parameter...\n'); fflush(stdout);
    
    t = [];
    for i = 1:length(tAlpha)
        t = [t; i * ones(size(tAlpha{i}, 2),1)];
    end
    tAlpha = cat(2, tAlpha{:});
    [~, Delta] = classifyHOSVD3(params, tAlpha);
    Delta = Delta - min(Delta);
    alpha = 1/std(Delta(:));
    opts = optimset('GradObj', 'on');
    alpha = fminunc(@(x) loglossSVD(Delta, t, x), alpha, opts);
    params.alpha = alpha;
end
