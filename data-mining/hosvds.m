function varargout = hosvds(T, k, r)
    varargout{1} = T;
    for i = 1:ndims(T)
        if exist('r')
            fprintf('Calculating SVD of mode %d/%d\n', i, ndims(T));
            fflush(stdout);
        end
        [varargout{i+1}, ~, ~] = svds(unfold(T, i), k);
        varargout{1} = tmul(varargout{1}, varargout{i+1}', i);
    end
end
