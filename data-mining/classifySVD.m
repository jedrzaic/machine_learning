function [ind, dist] = classifySVD(ps, test, conf)
    n = length(ps.U);
    ntest = size(test, 2);
    dist = zeros(n, ntest);
    for i = 1:n
        dist(i,:) = norm(test - ps.U(i).data*(ps.U(i).data'*test), 2, 'cols');
    end
    [~, ind] = min(dist, [], 1);
    if exist('conf')
        dist = exp(-ps.alpha*(dist - min(dist)));
    end
end
