function [cdata] = squashPeople(gdata)
    nc = size(gdata, 2);
    cdata = cell(nc, 1);
    for i = 1:nc
        cdata{i} = [gdata{:,i}];
    end
end
