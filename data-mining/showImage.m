function [] = showImages(images, indices, width, height)
    
    if exist('height') == 0
        height = size(images, 1) / width;
    end

    n = length(indices);
    
    if width >= height
        ydim = floor(sqrt(width/height * n));
        xdim = ceil(n / ydim);
    else
        xdim = floor(sqrt(height/width * n));
        ydim = ceil(n / xdim);
    end
    
    m = max(xdim, ydim);
    
    im = zeros(m * height, m * width);
    
    for i = 1:ydim
        for j = 1:xdim
            ind = (i-1)*ydim + j;
            if ind <= n
                r = (i-1)*height + 1;
                c = (j-1)*width + 1;
                im(r:r+height-1, c:c+width-1) = ...
                    reshape(images(:,indices(1) + ind - 1), height, width);
            end
        end
    end
    imagesc(im);
    colormap gray;

end
