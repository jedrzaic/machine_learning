function [A, dims] = unfold(T, mode)
    dims = size(T);
    if length(dims) < mode
        dims = [dims ones(1, mode - length(dims))];
    end
    m = size(T, mode);
    n = numel(T) / m;
    A = zeros(m, n);
    
    inds = num2cell(ones(size(dims)));
    inds{mode} = ':';
    
    for i = 1:n
        A(:,i) = T(inds{:});
        inds = nextind(inds, dims);
    end
end
