function [pred] = generatePredictions(clsfunc, files, outfile)
    pred = {'img' 'c0' 'c1' 'c2' 'c3' 'c4' 'c5' 'c6' 'c7' 'c8' 'c9'};
    n = length(files);
    for i = 1:n
        fprintf('Loading data block %d / %d\n', i, n); fflush(stdout);
        load(files{i}); % loads data & tnames
        data = double(data);
        tnames = tnames(1:size(data,2));
        fprintf('Running tests on block %d / %d\n', i, n); fflush(stdout);
        [~, conf] = clsfunc(data);
        conf = conf ./ sum(conf);
        tp = cat(2, tnames,num2cell(conf'));
        pred = cat(1, pred,tp);
    end
    fprintf('Saving predictions to %s...\t', outfile); fflush(stdout);
    try
        cell2csv(outfile, pred);
    catch
        fprintf('FAILED\nTrying to load module `io`...\n'); fflush(stdout);
        pkg load io;
        fprintf('Retrying...\t'); fflush(stdout);
        cell2csv(outfile, pred);
    end
    fprintf('DONE!\n'); fflush(stdout);
end