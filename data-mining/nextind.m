function [inds] = nextind(inds, dims)
    for j = 1:length(inds)
        if ischar(inds{j})
            continue
        end
        inds{j} = inds{j} + 1;
        if inds{j} > dims(j)
            inds{j} = 1;
        else
            break;
        end
    end
end
