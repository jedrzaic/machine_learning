function [R] = tmul(T, A, mode)
    [X, d] = unfold(T, mode);
    R = fold(A*X, d, mode);
end
