function [params] = trainHOSVD4Classifier (M, k, n, p)
    if ~exist('n') || length(n) == 0
        n = 3;
    end
    if ~exist('p') || length(p) == 0
        p = 0.1;
    end
    [train, tAlpha] = splitData(M, n, p, 0);
    tAlpha = squashPeople(tAlpha);
    
    T = zeros([size(train{1,1}) size(train)]);
    for i = 1:size(train, 1)
        for j = 1:size(train, 2)
            T(:,:,i,j) = train{i,j};
        end
    end
    
    fprintf('Calculating HOSVD...\n'); fflush(stdout);
    %[S, F, G, H, K] = hosvds(T, k, 0);
    [S, F, G, H, K] = hosvd(T, 0);
    
    params = struct('S', S, 'F', F, 'G', G, 'H', H, 'K', K, 'alpha', 1, 's', 1);
    
    fprintf('Calculating optimal distance loss parameter...\n'); fflush(stdout);
    
    t = [];
    for i = 1:length(tAlpha)
        t = [t; i * ones(size(tAlpha{i}, 2),1)];
    end
    tAlpha = cat(2, tAlpha{:});
    [~, Delta] = classifyHOSVD4(params, tAlpha);
    Delta = Delta - min(Delta);
    alpha = 1/std(Delta(:));
    opts = optimset('GradObj', 'on');
    alpha = fminunc(@(x) loglossSVD(Delta, t, x), alpha, opts);
    params.alpha = alpha;
end
