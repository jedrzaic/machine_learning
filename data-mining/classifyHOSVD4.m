function [ind, dist] = classifyHOSVD4(params, tests, conf)
    len = size(tests, 2);
    dist = zeros(size(params.K, 1), size(tests, 2));
    [uS, d] = unfold(params.S, 1);
    ts = fold((tests' * params.F) * pinv(uS)', [d(2:4) d(1)], 4);
    for i = 1:len
        fprintf("Classifying test %d/%d\r", i, len); fflush(stdout);
        R = ts(:,:,:,i);
        c = cell(3, 1);
        [~, c{1}, c{2}, c{3}] = hosvd(R);
        c{1} = c{1}(:,1);
        c{2} = c{2}(:,1);
        c{3} = c{3}(:,1);
        for j = 1:params.s
            for k = 1:3
                Rp = R;
                for l = 1:3
                    if l ~= k
                        Rp = tmul(Rp, c{l}', l);
                    end
                end
                c{k} = Rp(:);
                c{k} = c{k} / norm(c{k});
            end
        end
        %Rp = tmul(tmul(tmul(R, c{1}*c{1}', 1), c{2}*c{2}', 2), c{3}*c{3}', 3);
        %printf("Err %f\n", norm(R(:) - Rp(:)));
        dist(:,i) = norm(params.K - c{3}', 2, 'rows');
    end
    [~, ind] = min(dist, [], 1);
    if exist('conf')
        dist = exp(-params.alpha*(dist - min(dist)));
    end
end
    SSS