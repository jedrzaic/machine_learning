function [G] = groupData(data, pids, cids)
    npid = length(unique(pids));
    ncid = length(unique(cids));
    G = cell(npid, ncid);
    for i = 1:npid
        for j = 1:ncid
            G{i,j} = data(:, find(pids == i & cids == j));
        end
    end
end
