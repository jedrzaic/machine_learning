function [params] = trainSVDClassifier(M, k, n, p)
    
    if exist('n') == 0 || length(n) == 0
        n = 3;
    end
    if exist('p') == 0 || length(p) == 0
        p = 0.1;
    end
    
    [tSVD, tAlpha] = splitData(M, n, p);
    tSVD = squashPeople(tSVD);
    tAlpha = squashPeople(tAlpha);
    
    n = length(tSVD);
    U = cell(n, 1);

    for i = 1:n
        fprintf("Training class %d/%d", i, n);
        fflush(stdout);
        [U{i}, S, ~, flag] = svds(tSVD{i}, k);
        if flag == 0
            fprintf('\tConverged. min/max singular value ratio: %f\n', ...
                    S(k,k)/S(1,1));
        else
            fprintf('\tFailed to converge!\n');
        end
    end

    fprintf('Calculating optimal distance loss parameter...\n'); fflush(stdout);
    params = struct('U', struct('data', U), 'alpha', 0);
    
    t = [];
    for i = 1:length(tAlpha)
        t = [t; i * ones(size(tAlpha{i}, 2),1)];
    end
    tAlpha = cat(2, tAlpha{:});
    [~, Delta] = classifySVD(params, tAlpha);
    Delta = Delta - min(Delta);
    alpha = 1/std(Delta(:));
    opts = optimset('GradObj', 'on');
    alpha = fminunc(@(x) loglossSVD(Delta, t, x), alpha, opts);
    params.alpha = alpha;
end
