function [val, grad] = loglossSVD(Delta, t, alpha)
    N = size(Delta, 2);
    fv = exp(-alpha*Delta);
    cfv = sum(fv);
    cdfv = sum(Delta .* fv);
    pv = fv ./ cfv;
    ti = sub2ind(size(Delta), t, [1:N]');
    val = -1/N * sum(log(pv(ti)));
    grad = -1/N * sum(cdfv ./ cfv - Delta(ti)');
end
