function [T] = fold(A, dims, mode)
    dims(mode) = size(A, 1);
    
    T = zeros(dims);
    
    inds = num2cell(ones(size(dims)));
    inds{mode} = ':';
    
    for i = 1:size(A,2)
        T(inds{:}) = A(:,i);
        inds = nextind(inds, dims);
    end
end
