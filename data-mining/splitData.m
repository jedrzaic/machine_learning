function [train, valid] = splitData(gdata, pvalid, ratio, equal)
    if exist('equal')
        minels = min(min(cellfun(@(x) size(x, 2), gdata)));
        split = floor(minels * (1-ratio));
    end
    plen = size(gdata, 1);
    clen = size(gdata, 2);
    pperm = randperm(plen);
    train = cell(plen - pvalid, clen);
    valid = cell(size(gdata));
    valid(pperm(1:pvalid), :) = gdata(pperm(1:pvalid), :);
    
    for i = pvalid+1:plen
        p = pperm(i);
        for j = 1:clen
            len = size(gdata{p, j}, 2);
            if ~exist('equal')
                split = floor(len * (1-ratio));
            end
            perm = randperm(len);
            train{i-pvalid,j} = gdata{p,j}(:, perm(1:split));
            valid{p,j} = gdata{p,j}(:, perm(split+1:end));
        end
    end
    
end
