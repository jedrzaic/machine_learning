function varargout = hosvd(T, o)
    varargout{1} = T;
    for i = 1:ndims(T)
        if exist('o')
            fprintf('Calculating SVD of mode %d/%d\n', i, ndims(T));
            fflush(stdout);
        end
        [varargout{i+1}, ~, ~] = svd(unfold(T, i), 0);
        varargout{1} = tmul(varargout{1}, varargout{i+1}', i);
    end
end
