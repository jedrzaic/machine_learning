import utils
import vgg_16

from keras.optimizers import SGD 
from keras.layers.core import Dense

def my_vgg_16_base(img_rows, img_cols, color_type_global, pretrained = True):
    if (color_type_global, img_rows, img_cols) != (3, 224, 224):
        raise Exeption("image size must be (224 x 224)")
    if pretrained:
        model = vgg_16.VGG_16('vgg16_weights.h5')
    else:
        model = vgg_16.VGG_16(None)
    model.layers.pop()
    model.add(Dense(10, activation='softmax'))
    sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy')
    return model

def my_vgg_16_pret(img_rows, img_cols, color_type_global):
    return my_vgg_16_base(img_rows, img_cols, color_type_global, True)


def my_vgg_16(img_rows, img_cols, color_type_global):
    return my_vgg_16_base(img_rows, img_cols, color_type_global, False)

if __name__ == "__main__":
    #utils.run_single(utils.create_model_v1h)
    utils.run_single(my_vgg_16_pret, img_rows=224, img_cols=224, cname='vgg16', nb_epoch=5,
		     batch_size = 64, predict=True)
    #utils.run_single(my_vgg_16_pret_notrain,
    #                 img_rows=224, img_cols=224,
    #                 cname='vgg16nt', nb_epoch=10)
    #utils.run_single(my_vgg_16,
    #                 img_rows=224, img_cols=224,
    #                 cname='vgg16ntnp', nb_epoch=10)
    #utils.run_single(utils.create_model_v1, nb_epoch=4, cname='simple', predict=True)


