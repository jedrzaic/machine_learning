�
��AWc           @   s�  d  d l  Z e j j d � d  d l Z d  d l Z d  d l Z d  d l Z d  d l Z d  d l	 Z	 d  d l
 Z d  d l Z d  d l Z d  d l m Z d  d l Z d  d l Z e j d � d  d l m Z d  d l m Z d  d l m Z d  d l m Z m Z m Z m Z d  d	 l m Z m Z m Z d  d
 l  m! Z! d  d l" m# Z# d  d l" m$ Z$ d  d l% m& Z& m' Z' d  d l( m) Z) d  d l m* Z* d  d l+ m, Z, d  d l- m. Z. m/ Z/ m0 Z0 d Z1 d d � Z2 d d � Z3 d d � Z4 d �  Z5 d d � Z6 d d � Z7 d �  Z8 d �  Z9 d �  Z: d �  Z; d �  Z< d �  Z= d  �  Z> d d! � Z? d d" � Z@ d# �  ZA d$ �  ZB d% �  ZC d d& � ZD d' �  ZE d( d) d) d* d+ d, d- d. � ZF d) d) d* d/ d, d0 eG d1 � ZH eI d2 k r�eH eD � n  d S(3   i����Ni�  (   t   copy2t   ignore(   t   train_test_split(   t   KFold(   t
   Sequential(   t   Denset   Dropoutt
   Activationt   Flatten(   t   Convolution2Dt   MaxPooling2Dt   ZeroPadding2D(   t   BatchNormalization(   t   SGD(   t   Adam(   t   EarlyStoppingt   ModelCheckpoint(   t   np_utils(   t   model_from_json(   t   log_loss(   t   imreadt   imresizet   imshowi   t   imagec         C   s+   t  j | |  � t  j d � t  j �  d  S(   Ni    (   t   cv2R   t   waitKeyt   destroyAllWindows(   t   imt   name(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   show_image%   s    c         C   sa   | d k r! t  j |  d � } n | d k r? t  j |  � } n  t  j | | | f t  j � } | S(   Ni   i    i   (   R   R   t   resizet   INTER_LINEAR(   t   patht   img_rowst   img_colst
   color_typet   imgt   resized(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   get_im_cv2,   s    c         C   sR   | d k r! t  j |  d � } n t  j |  � } t  j | | | f t  j � } | S(   Ni   i    (   R   R   R   R   (   R    R!   R"   R#   R$   R%   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   get_im_cv2_mod7   s
    c          C   s  t  �  }  t  �  } t j j d d d � } d GHt | d � } | j �  } x� | j �  } | d k ri Pn  | j �  j d � } | d |  | d	 <| d | j �  k r� | d
 | d	 f g | | d <qM | | d j	 | d
 | d	 f � qM W| j
 �  |  | f S(   Ns   ..t   inputs   driver_imgs_list.csvs   Read drivers datat   rt    t   ,i    i   i   (   t   dictt   osR    t   joint   opent   readlinet   stript   splitt   keyst   appendt   close(   t   drt   clssR    t   ft   linet   arr(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   get_driver_dataE   s"    		"'
c         C   si  g  } g  } g  } g  } t  j  �  } t �  \ } }	 d GHx� t d � D]� }
 d j |
 � GHt j j d d d d t |
 � d � } t j | � } xm | D]e } t j j	 | � } t
 | |  | | � } | j | � | j | � | j |
 � | j | | � q� WqE Wd	 j t t  j  �  | d
 � � GHt t t | � � � } d j t | � � GH| GH| | | | | f S(   Ns   Read train imagesi
   s   Load folder c{}s   ..R(   t   traint   cs   *.jpgs    Read train data time: {} secondsi   s   Unique drivers: {}(   t   timeR;   t   ranget   formatR-   R    R.   t   strt   globt   basenameR'   R4   t   roundt   sortedt   listt   sett   len(   R!   R"   R#   t   X_traint
   X_train_idt   y_traint	   driver_idt
   start_timet   driver_datat   dr_classt   jR    t   filest   flt   flbaseR$   t   unique_drivers(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   load_trainZ   s.    (!c         C   s  d GHt  j j d d d d � } t j | � } g  } g  } d } t j �  } t j t | � d � }	 x� | D]{ }
 t  j j |
 � } t	 |
 |  | | � } | j
 | � | j
 | � | d 7} | |	 d k rm d	 j | t | � � GHqm qm Wd
 j t t j �  | d � � GH| | f S(   Ns   Read test imagess   ..R(   t   tests   *.jpgi    i
   i   s   Read {} images from {}s   Read test data time: {} secondsi   (   R-   R    R.   RB   R>   t   matht   floorRH   RC   R'   R4   R@   RD   (   R!   R"   R#   R    RQ   t   X_testt	   X_test_idt   totalRM   t   thrRR   RS   R$   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt	   load_testw   s$    
!c         C   sS   t  j j t  j j | � � rJ t | d � } t j |  | � | j �  n d GHd  S(   Nt   wbs   Directory doesnt exists(   R-   R    t   isdirt   dirnameR/   t   picklet   dumpR5   (   t   dataR    t   file(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   cache_data�   s
    c         C   s@   t  �  } t j j |  � r< t |  d � } t j | � } n  | S(   Nt   rb(   R,   R-   R    t   isfileR/   Ra   t   load(   R    Rc   Rd   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   restore_data�   s
    	c         C   s[   |  j  �  } t j j d � s. t j d � n  t | d � j | � |  j | d t �d  S(   Nt   cachet   wt	   overwrite(	   t   to_jsonR-   R    R_   t   mkdirR/   t   writet   save_weightst   True(   t   modelt	   arch_patht   weights_patht   json_string(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   save_model�   s
    c         C   s)   t  t |  � j �  � } | j | � | S(   N(   R   R/   t   readt   load_weights(   Rs   Rt   Rr   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   read_model�   s    c         C   s=   d } t  |  | d | d | �\ } } } } | | | | f S(   Ni3   t	   test_sizet   random_state(   R   (   R<   t   targetRz   R{   RI   RY   RK   t   y_test(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   split_validation_set�   s    'c         C   s�   t  j |  d d d d d d d d d	 d
 d g
 �} t  j | d | j �| j d  d  � d f <t j j �  } t j j	 d � s� t j
 d � n  | d t | j d � � } t j j d d | d � } | j | d t �d  S(   Nt   columnst   c0t   c1t   c2t   c3t   c4t   c5t   c6t   c7t   c8t   c9t   indexR$   t   submt   _s   %Y-%m-%d-%H-%Mt   submission_s   .csv(   t   pdt	   DataFramet   SeriesR�   t   loct   datetimet   nowR-   R    R_   Rn   RA   t   strftimeR.   t   to_csvt   False(   t   predictionst   test_idt   infot   result1R�   t   suffixt   sub_file(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   create_submission�   s    3+c         C   s�  t  j |  d d d d d d d d d	 d
 d g
 �} t  j | d | j �| j d  d  � d f <t j j �  } t j j	 t j j
 d d � � s� t j t j j
 d d � � n  | d t | j d � � } t j j
 d d d | d � } | j | d t �| j �  } t j j
 d d d | d � }	 t |	 d � j | � t j j t � }
 t j j
 d d d | d � } t |
 | � d  S(   NR   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   R$   R�   Rc   R�   s   %Y-%m-%d-%H-%Mt   s_s   _train_predictions.csvs   _model.jsonRk   s   _code.py(   R�   R�   R�   R�   R�   R�   R�   R-   R    R_   R.   Rn   RA   R�   R�   R�   Rm   R/   Ro   t   realpatht   __file__R    (   t   predictions_validt	   valid_idsRr   R�   R�   R�   R�   t	   pred_fileRu   t
   model_filet   cur_codet	   code_file(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   save_useful_data�   s    3+!   c   	      C   s�  t  j j d d t |  � d t | � d t | � d � } t  j j | � s^ t d k r� t |  | | � \ } } } } } t | | | | | f | � n  d GHt | � \ } } } } } t	 j
 | d t	 j �} t	 j
 | d t	 j �} | d	 k r| j | j d d	 |  | � } n | j d � } t j | d � } | j d � } | d :} d | j f GH| j d d f GH| | | | | f S(   NRj   t   train_r_t   _c_t   _t_s   .dati    s   Restore train from cache!t   dtypei   i   i   i
   t   float32i�   s   Train shape:s   train samples(   i    i   i   i   (   R-   R    R.   RA   Rg   t	   use_cacheRU   Re   Ri   t   npt   arrayt   uint8t   reshapet   shapet	   transposeR   t   to_categoricalt   astype(	   R!   R"   R#   t
   cache_patht
   train_datat   train_targett   train_idRL   RT   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   read_and_normalize_train_data�   s"    ?!"
c         C   s;  t  j j d d t |  � d t | � d t | � d � } t  j j | � s^ t d k r� t |  | | � \ } } t | | f | � n d GHt | � \ } } t	 j
 | d t	 j �} | d	 k r� | j | j d d	 |  | � } n | j d � } | j d � } | d :} d | j f GH| j d d f GH| | f S(   NRj   t   test_r_R�   R�   s   .dati    s   Restore test from cache!R�   i   i   i   R�   i�   s   Test shape:s   test samples(   i    i   i   i   (   R-   R    R.   RA   Rg   R�   R]   Re   Ri   R�   R�   R�   R�   R�   R�   R�   (   R!   R"   R#   R�   t	   test_dataR�   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   read_and_normalize_test_data�   s    ?"
c         C   sX   t  j |  d � } x. t d | � D] } | t  j |  | � 7} q# W| | :} | j �  S(   Ni    i   (   R�   R�   R?   t   tolist(   Rc   t   nfoldst   at   i(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   merge_several_folds_mean  s
    
c         C   sd   t  j |  d � } x. t d | � D] } | t  j |  | � 9} q# Wt  j | d | � } | j �  S(   Ni    i   (   R�   R�   R?   t   powerR�   (   Rc   R�   R�   R�   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   merge_several_folds_geom	  s
    c         C   s�   g  } g  } g  } x\ t  t | � � D]H } | | | k r% | j |  | � | j | | � | j | � q% q% Wt j | � } t j | � } t j | � } | | | f S(   N(   R?   RH   R4   R�   R�   (   R�   R�   RL   t   driver_listRc   R|   R�   R�   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   copy_selected_drivers  s    c         C   sX  t  �  } | j t d d d d d d d d | |  | f �� | j t d d � � | j t d
 � � | j t d d d d d d d �� | j t d d � � | j t d
 � � | j t d d d d d d d �� | j t d d � � | j t d
 � � | j t �  � | j t d � � | j t d � � | j t	 d d � d d �| S(   Ni    i   t   border_modet   samet   initt	   he_normalt   input_shapet	   pool_sizei   g      �?i@   i�   i   i
   t   softmaxt   lrg����MbP?t   losst   categorical_crossentropy(   i   i   (   i   i   (   i   i   (
   R   t   addR	   R
   R   R   R   R   t   compileR   (   R!   R"   R#   Rr   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   create_model_v1   s     	!%%c         C   s8   g  } x+ t  t |  � � D] } | j | | � q W| S(   N(   R?   RH   R4   (   R�   R�   t   pvR�   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   get_validation_predictions7  s    i
   i@   i   i   i   i    c   '      C   s�  t  | | | � \ } } }	 }
 } t | | | � \ } } t | | | � } t �  } g  } t t | � d |  d t d t �} d } d } xi| D]a\ } } g  | D] } | | ^ q� } t | | |
 | � \ } } } g  | D] } | | ^ q� } t | | |
 | � \ } } } | d 7} d j	 | |  � GHd t | � t | � f GHd t | � t | � f GHd	 | f GHd
 | f GHt
 j j d d t | � d � } t
 j j | � s�| d k r0t d d d d d d � t | d d d t d d �g }  | j | | d | d | d t d d d | | f d |  �n  t
 j j | � rR| j | � n  | j | d | d d �}! t | |! � }" d |" f GH| |" t | � 7} x, t t | � � D] } |! | | | | <q�W| j | d | d d �}# | j |# � q� W| t | � }" d |" f GHt | | � }! t | |! � }$ t |$ |" � d k rYd j	 |" |$ � GHn  d j	 |" | | |  | � GHd t |" � d t | � d t | � d t |  � d  t | � }% t | |  � }& t |& | |% � t |! |	 | |% � d  S(!   Nt   n_foldst   shuffleR{   i    i   s   Start KFold number {} from {}s   Split train: s   Split valid: s   Train drivers: s   Test drivers: Rj   t   weights_kfold_s   .h5t   monitort   val_losst   patiencet   verboset   save_best_onlyt
   batch_sizet   nb_epocht   validation_datat	   callbackss   Score log_loss: s    Log_loss train independent avg: g-C��6?s   Check error: {} != {}s:   Final log_loss: {}, rows: {} cols: {} nfolds: {} epoch: {}t   loss_t   _r_R�   t   _folds_t   _ep_(   R�   R�   R�   R,   R   RH   Rq   R{   R�   R@   R-   R    R.   RA   Rg   R   R   t   fitRx   t   predictR   R?   R4   R�   t   absR�   R�   R�   ('   R�   R!   R"   t   color_type_globalR�   R�   t   restore_from_last_checkpointR�   R�   R�   RL   RT   R�   R�   Rr   t   yfull_traint
   yfull_testt   kft   num_foldt	   sum_scoret   train_driverst   test_driversR�   t   unique_list_trainRI   t   Y_traint   train_indext   unique_list_validt   X_validt   Y_validt
   test_indext   kfold_weights_pathR�   R�   t   scoret   test_predictiont   score1t   info_stringt   test_res(    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   run_cross_validation>  s^    !	$
#!
Hi    t   simplec   "      C   s�  d } t  | | | � \ }	 }
 } } } d d d d d d d d	 d
 d d d d d d d d d d d d d g } t |	 |
 | | � \ } } } d d d d g } t |	 |
 | | � \ } } } d GHd t | � f GHd t | � f GHd | f GHd  | f GHt j j d! d" | d# � } t d$ d% d& d' d( d) � t | d$ d% d* t d( d) �g } |  | | | � } t j j	 | � r�| r�| j
 | � n  | j | | d+ | d, | d- t d( d. d/ | | f d0 | �| j | d+ | d( d. �} t | | � } d1 | f GHt | | | � \ } } g  } | j | d+ | d( d. �} | j | � d2 j | | | | � GHd3 t | � d4 t | � d5 t | � d6 t | � } | j |	 d+ | d( d. �}  t |
 |  � } d7 | f GHt | d. � }! t |! | | � t |  | | | � d  S(8   Ni3   t   p002t   p012t   p014t   p015t   p016t   p021t   p022t   p035t   p041t   p042t   p045t   p047t   p049t   p050t   p051t   p052t   p056t   p061t   p064t   p066t   p075t   p081t   p024t   p026t   p039t   p072s   Start Single Runs   Split train: s   Split valid: s   Train drivers: s   Valid drivers: Rj   t   weights_s   .h5R�   R�   R�   i   R�   i    R�   R�   R�   R�   i   R�   R�   s   Score log_loss: s/   Final log_loss: {}, rows: {} cols: {} epoch: {}R�   R�   R�   R�   s   Full score log_loss: (   R�   R�   RH   R-   R    R.   R   R   Rq   Rg   Rx   R�   R�   R   R�   R4   R@   RA   R�   R�   R�   ("   t
   model_funcR!   R"   R�   R�   R�   t   cnamet   restoreR{   R�   R�   R�   RL   RT   R�   RI   R�   R�   R�   R�   R�   R�   Rt   R�   Rr   R�   R�   R�   R�   R�   R�   R�   t	   full_predR�   (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt
   run_single�  sF    !H!:t   __main__(J   t   numpyR�   t   randomt   seedR-   RB   R   RW   Ra   R�   t   pandasR�   t
   statisticsR>   t   shutilR    t   warningst   filterwarningst   sklearn.cross_validationR   R   t   keras.modelsR   t   keras.layers.coreR   R   R   R   t   keras.layers.convolutionalR	   R
   R   t   keras.layers.normalizationR   t   keras.optimizersR   R   t   keras.callbacksR   R   t   keras.utilsR   R   t   sklearn.metricsR   t
   scipy.miscR   R   R   R�   R   R&   R'   R;   RU   R]   Re   Ri   Rv   Ry   R~   R�   R�   R�   R�   R�   R�   R�   R�   R�   R�   Rq   R   t   __name__(    (    (    s0   /home/gflegar/code/distracted-driver/ml/utils.pyt   <module>   sp   "															P		=