from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.regularizers import l2

import utils2


def gjs_model_v2(img_rows, img_cols, color_type=3):
    reg = l2(0.02)
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg,
                            input_shape=(color_type, img_rows, img_cols)))
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))
    #--------------------------------------- 32x32x32
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))
    #--------------------------------------- 16x16x64
    
    model.add(Convolution2D(128, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(64, activation='relu', W_regularizer=reg))
    model.add(Dense(10, activation='softmax', W_regularizer=reg))

    sgd = SGD(lr=1e-3, decay=1e-16, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy')
    return model

def gjs_model_v3(img_rows, img_cols, color_type=3):
    reg = l2(0.0)
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg,
                            input_shape=(color_type, img_rows, img_cols)))
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))
    #--------------------------------------- 32x32x32
    
    #model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
    #                        activation='relu', W_regularizer=reg))
    #model.add(Convolution2D(64, 3, 3, borde(128, 3, 3, border_mode='same', init='he_normal',
    #                        activation='relu', W_regularizer=reg))
    #model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
    #                        activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(256, activation='relu', W_regularizer=reg))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax', W_regularizer=reg))

    sgd = SGD(lr=1e-4, decay=1e-16, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy')
    return model


if __name__ == "__main__": 
    utils2.run_single(gjs_model_v2, img_rows=64, img_cols=64, cname='jgl8v17', nb_epoch=15,
                     batch_size=64, predict=True)
