from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.regularizers import l2

import utils2


def gjs_model(img_rows, img_cols, color_type=3):
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu',
                            input_shape=(color_type, img_rows, img_cols)))
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    #model.add(Dropout(0.5))
    #--------------------------------------- 32x32x32
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu'))
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu'))
    model.add(MaxPooling2D(pool_size=(8,8)))
    #--------------------------------------- 4x4x64
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax'))

    sgd = SGD(lr=1e-2, decay=1e-16, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy')
    return model


def gjs_model_v2(img_rows, img_cols, color_type=3):
    reg = l2(0.0)
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg,
                            input_shape=(color_type, img_rows, img_cols)))
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.5))
    #--------------------------------------- 32x32x32
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            activation='relu', W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(8,8)))
    #--------------------------------------- 4x4x64
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(64, activation='relu', W_regularizer=reg))
    model.add(Dropout(0.5))
    model.add(Dense(10, activation='softmax', W_regularizer=reg))

    sgd = SGD(lr=1e-2, decay=1e-16, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy')
    return model


if __name__ == "__main__":
    #utils2.run_single(gjs_model, img_rows=64, img_cols=64, cname='jgl6v3', nb_epoch=10,
    #                  batch_size=64, predict=True)
 
    utils2.run_single(gjs_model_v2, img_rows=64, img_cols=64, cname='jgl6v7', nb_epoch=0,
                      batch_size=64, predict=True)
