# -*- coding: utf-8 -*-

import numpy as np
np.random.seed(2016)

import os
import glob
import cv2
import math
import pickle
import datetime
import pandas as pd
import statistics
import time
from shutil import copy2
import warnings
import random
warnings.filterwarnings("ignore")

from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import KFold
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils import np_utils
from keras.models import model_from_json
from sklearn.metrics import log_loss
from scipy.misc import imread, imresize, imshow

from keras.regularizers import l2

use_cache = 1

import utils2

def create_model_v1(img_rows, img_cols, color_type=1):
    reg = l2(0.0)
    model = Sequential()
    model.add(Convolution2D(32, 3, 3, border_mode='same', init='he_normal',
			    W_regularizer=reg,
                            input_shape=(color_type, img_rows, img_cols)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.50))

    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
			    W_regularizer=reg))
    model.add(Convolution2D(64, 3, 3, border_mode='same', init='he_normal',
                            W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.50))

    model.add(Convolution2D(128, 3, 3, border_mode='same', init='he_normal',
                            W_regularizer=reg))
    model.add(MaxPooling2D(pool_size=(8, 8)))
    model.add(Dropout(0.50))

    model.add(Flatten())
    model.add(Dense(30, W_regularizer=reg))
    model.add(Dense(10, W_regularizer=reg))
    model.add(Activation('softmax'))

    model.compile(Adam(lr=1e-3), loss='categorical_crossentropy')
    return model


if __name__ == '__main__':
    utils2.run_single(create_model_v1, img_rows=64, img_cols=64, color_type_global=3,
                      batch_size=64, nb_epoch=7, predict=True, cname='jm61')

