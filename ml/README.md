Prepoznavanje ometenih vozača
=============================

Ovaj repozitorij sadrži izvorni kod za Kaggle challenge "Distracted driver detection".
Kod je nastao u sklopu projekta za kolegij "Strojno učenje".

Za pokretanje je potreban *python* (testirano na verziji 2.7).

Potrebne biblioteke:

* *opencv* -- za učitavanje podataka
* *keras* -- biblioteka za neuronske mreže
* *tensorflow* ili *theano* -- backend za *keras*
* *scikit-learn* -- računanje metrika (logloss, točnost, itd.)

Preuzeti izvorni kod (koji se nalazi i u ovom repozitoriju):

* *utils.py* i *utils2.py* -- učitavanje i obrada podataka za ovaj challenge (dostupna i s Kaggle stranice)
* *vgg_16.py* -- Keras implementacije modela VGG16

Naši kodovi:

* *my\_vgg\_16.py* -- adapter između *vgg_16.py* i *utils.py*, pokreće treniranje modela VGG16
* *jgl_model.py* -- pokreće treniranje modela JGL6
* *jgl_model2.py*, *simple_model.py* -- još neki isprobani modeli

Pokretanje programa:
--------------------

Programi se pokreću iz terminala naredbama:

    $ python2 my_vgg_16.py
    $ python2 jgl_model.py

Za rad programa potrebne su i sljedeće datoteke na navedenim relativnim putovima:

    ../input/driver_imgs_list.csv     # preuzima se s Kaggle-a
    ../input/train/                   # mapa s podacima za treniranje preuzeta s Kaggle-a
    ../input/test/                    # mapa s podacima za testiranje preuzeta s Kaggle-a
    ./vgg_16_weights.h5               # početne težine za VGG16

Početne težine za VGG16 mogu se preuzeti [ovdje](https://drive.google.com/file/d/0Bz7KyqmuGsilT0J5dmRCM0ROVHc/view).
  
